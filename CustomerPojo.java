package com.healthinsurence.pojo;

public class CustomerPojo {
	
	private String name;
	private String gender;
	private int age;
	private String currentHealth;
	private String habits;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(String currentHealth) {
		this.currentHealth = currentHealth;
	}
	public String getHabits() {
		return habits;
	}
	public void setHabits(String habits) {
		this.habits = habits;
	}
	
	public CustomerPojo(String name, String gender, int age, String currentHealth, String habits) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currentHealth = currentHealth;
		this.habits = habits;
	}
	@Override
	public String toString() {
		return "CustomerPojo [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth=" + currentHealth
				+ ", habits=" + habits + "]";
	}
}
