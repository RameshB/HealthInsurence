<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form:form id="customerForm" modelAttribute="CustomerPojo"
		action="customerProcess" method="post">
		<table align="center">
			<tr>
				<td><form:label path="name"> Name:Norman Gomes </form:label></td>
				<td><form:input path="name" name="name" id="name" /></td>
			</tr>
			<tr>
				<td><form:label path="gender">Gender:Male</form:label></td>
				<td><form:password path="gender" name="gender" id="gender" />
				</td>
			</tr>
			<tr>
				<td><form:label path="age">Age:34 years</form:label></td>
				<td><form:password path="age" name="age" id="age" /></td>
			</tr>

			<tr>
				<td><form:label path="currentHealth">CurrentHealth:</form:label>
				</td>
				<td><form:password path="currentHealth" name="currentHealth"
						id="currentHealth" /></td>
				<input type="radio" name="Hypertension" value="Hypertension">
				Hypertension: No
				<br>
				<input type="radio" name="Blood pressure" value="Blood pressure">
				Blood pressure: No
				<br>
				<input type="radio" name="Blood sugar" value="Blood sugar">
				Blood sugar: No
				<br>
				<input type="radio" name="BOverweight" value="BOverweight">
				Overweight: Yes

			</tr>
			<tr>
				<td><form:label path="habits">Habits:</form:label></td>
				<td><form:password path="habits" name="habits" id="habits" />
				</td>
				<input type="radio" name="Smoking" value="Smoking"> Smoking:
				No
				<br>
				<input type="radio" name="Alcohol" value="Alcohol"> Alcohol:
				Yes
				<br>
				<input type="radio" name="Dailyexercise" value="Dailyexercise">
				Daily exercise: Yes
				<br>
				<input type="radio" name="Drugs" value="Drugs"> Drugs: No

			</tr>
			<tr>
				<td></td>
				<td align="left"><form:button id="submit" name="submit">Submit</form:button>
				</td>
			</tr>
			<tr></tr>

		</table>
	</form:form>
	<table align="center">
		<tr>
			<td style="font-style: italic; color: red;">${message}</td>
		</tr>
	</table>
</body>
</html>